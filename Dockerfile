FROM openjdk:8-jre-alpine3.9

COPY /target/Api-Investimentos-0.0.1-SNAPSHOT.jar /Api-Investimentos-0.0.1-SNAPSHOT.jar

# Para usar os argumentos (credenciais do mysql) que foram passados pelo Jenkins como argumentos
# do comando docker.build, precisamos receber esse argumentos dentro do Dockerfile definindo 
# através do comando ARG qual o nome de cada argumento passado.
ARG MYSQL_HOST
ARG MYSQL_PORT
ARG MYSQL_USER
ARG MYSQL_PASS

# Com o bloco comando ENV, definimos que os valores dos argumentos passados no comando docker.build e
# recebidos pelos comandos ARG, serão utilizados para definir variáveis de ambiente que serão acessíveis
# em tempo de execução do container (após do docker run). Por exemplo, dizemos que dentro do container
# existirá uma variável de ambiente chamada mysql_host que armazena o valor passado por argumento através
# do arg $mysql_host.
ENV MYSQL_HOST=$MYSQL_HOST MYSQL_PORT=$MYSQL_PORT MYSQL_USER=$MYSQL_USER MYSQL_PASS=$MYSQL_PASS


CMD ["java", "-jar", "/Api-Investimentos-0.0.1-SNAPSHOT.jar"]
